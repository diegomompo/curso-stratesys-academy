package com.stratesys.servicecart.services;

import com.stratesys.servicecart.dtos.CartDto;
import com.stratesys.servicecart.dtos.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService{

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<CartDto> findAll() {
        List<ProductDto> products = Arrays.asList(restTemplate.getForObject("http://localhost:8001/product/all", ProductDto[].class));
        products.stream().map(product -> new CartDto(product, 1)).collect(Collectors.toList());
        return null;
    }
    @Override
    public CartDto findById(Long id, Integer amount) {
        Map<String, String> pathVariables = new HashMap<String, String>();
        pathVariables.put("id", id.toString());
        ProductDto product = restTemplate.getForObject("http://localhost:8001/product/by/{id}", ProductDto.class, pathVariables);
        return new CartDto(product, amount);
    }

}
