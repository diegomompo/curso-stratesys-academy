package com.stratesys.servicecart.controller;

import com.stratesys.servicecart.dtos.CartDto;
import com.stratesys.servicecart.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping("/all")
    public List<CartDto> findAll(){
        return cartService.findAll();
    }

    @GetMapping("/by/{id}/amount/{amount}")
    public CartDto findById(@PathVariable Long id, @PathVariable Integer amount) {
        return cartService.findById(id, amount);
    }
}
