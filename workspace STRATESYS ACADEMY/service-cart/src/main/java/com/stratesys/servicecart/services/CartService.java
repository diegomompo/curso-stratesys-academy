package com.stratesys.servicecart.services;

import com.stratesys.servicecart.dtos.CartDto;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;

public interface CartService {

    public List<CartDto> findAll();

    CartDto findById(Long id, Integer amount);
}
