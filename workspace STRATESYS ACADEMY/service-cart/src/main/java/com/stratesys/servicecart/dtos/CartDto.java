package com.stratesys.servicecart.dtos;

import java.util.Objects;

public class CartDto {

    public CartDto(){

    }

    public CartDto(ProductDto productDto, Integer amount) {
        this.productDto = productDto;
        this.amount = amount;
    }

    private ProductDto productDto;
    private Integer amount;

    public ProductDto getProductDto() {
        return productDto;
    }

    public void setProductDto(ProductDto productDto) {
        this.productDto = productDto;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartDto cartDto = (CartDto) o;
        return productDto.equals(cartDto.productDto) && amount.equals(cartDto.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productDto, amount);
    }

    @Override
    public String toString() {
        return "CartDto{" +
                "productDto=" + productDto +
                ", amount=" + amount +
                '}';
    }

    public Double getTotal(){
        return productDto.getPrice() * amount.doubleValue();
    }
}
