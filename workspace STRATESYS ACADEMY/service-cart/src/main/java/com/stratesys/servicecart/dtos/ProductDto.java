package com.stratesys.servicecart.dtos;

import java.util.Date;
import java.util.Objects;

public class ProductDto {

    private Long id;
    private String name;
    private Double price;
    private Date createAt;
    private Integer port;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDto that = (ProductDto) o;
        return id.equals(that.id) && name.equals(that.name) && price.equals(that.price) && createAt.equals(that.createAt) && port.equals(that.port);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, createAt, port);
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", createAt=" + createAt +
                ", port=" + port +
                '}';
    }
}
