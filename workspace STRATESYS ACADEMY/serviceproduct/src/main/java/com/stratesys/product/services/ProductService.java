package com.stratesys.product.services;

import com.stratesys.product.models.entity.Product;

import java.util.List;

public interface ProductService {
    public List<Product> findAll();
    public Product findById(Long id);
}
